import pandas as pd
import plot_likert as pl
import matplotlib.pyplot as plt

# Load data
CIPHE_answers = pd.read_json("exp3_news_data.json")
keywords_answers = pd.read_json("exp1_news_data.json")
characteristic = "interest"
# characteristic = "emotion"
# characteristic = "opinion"

# Define pairs and create an empty DataFrame
news_pairs = [
    (23, 4, "UK Riots"),
    (26, 5, "Recipes"),
    (24, 1, "US Politics"),
    (22, 3, "Phones"),
    (28, 6, "T. Swift"),
    (25, 2, "Space"),
    (27, 7, "Random"),
]
yelp_pairs = [
    (8, 1, "Mexican Rest."),
    (9, 2, "Tours"),
    (10, 3, "Vetrinary"),
    (11, 4, "Markets"),
    (12, 5, "Hotels"),
    (13, 6, "Negative Rest."),
    (14, 7, "Random"),
]
wiki_pairs = [
    (1, 1, "Religious"),
    (2, 2, "Author"),
    (3, 3, "Music"),
    (4, 4, "Swimming"),
    (5, 5, "Military"),
    (6, 6, "Badminton."),
    (7, 7, "Random"),
]

pairs = news_pairs

# Loop through each pair of cluster labels
C_df = pd.DataFrame()
for idx, (i_c, i_i, name) in enumerate(pairs):
    # Select 'opinion' responses for CIPHE and keyword clusters
    c_df = CIPHE_answers["likert_answers"][CIPHE_answers.cluster_id == i_c]
    i_df = keywords_answers["likert_answers"][keywords_answers.cluster_id == i_i]

    # Ensure data is extracted properly
    if not c_df.empty and not i_df.empty:
        # Ensure both 'opinion' lists have the same length by truncating to the minimum length
        min_length = min(
            len(c_df.iloc[0][characteristic]), len(i_df.iloc[0][characteristic])
        )
        c_opinion = c_df.iloc[0][characteristic][:min_length]
        i_opinion = i_df.iloc[0][characteristic][:min_length]

        # Create DataFrame with 'opinion' data for both CIPHE and keywords
        C_df[f"CIPHE_{name}"] = c_opinion
        C_df[f"KW_{name}"] = i_opinion

# Adjust layout and show the figure
scale_options = ["strongly_disagree", "disagree", "neutral", "agree", "strongly_agree"]
fig, ax=plt.subplots()
pl.plot_likert(
    C_df, scale_options, colors=pl.colors.likert5, width=0.8, figsize=(5, 4), legend=0, ax=ax
)
# axes[idx].tick_params(axis='y', rotation=65)
# axes[idx].set_title(f"CIPHE {i_c} vs Keywords {i_i}")

data = C_df
questions = list(data.keys())
for i in range(0, len(questions), 2):
    ax.axhspan(i -0.5, i + 1.5, color='lightgray', alpha=0.2)
    if i+2 <len(questions):
        ax.axhline(i +1.5, color='gray', linestyle='dashed', linewidth=0.7)

print(plt.xlim())
plt.xlabel(None)
plt.xticks([])
plt.tight_layout()
plt.title(characteristic)


plt.savefig(f"likert_news_{characteristic}.pdf", format="pdf", bbox_inches="tight")
plt.show()