from nltk.metrics import agreement as agree
import numpy as np

matrix = [
    [1,1,1,1,1,1,1,1,1,1],
    [1,1,1,1,1,1,1,1,1,1],
    [1,1,1,1,1,1,1,1,1,1],
    # [1,1,1,1,1],
    # [1,0,1,0,1],
    # [0,1,0,1,0],
    # [1,1,1,1,1],
    # [1,1,1,1,1],
    [1,1,1,1,1,1,1,1,1,0]
]
matrix = [[np.random.randint(0,2) for _ in range(20)] for _ in range(20)]

responses = []
for user_id, ary in enumerate(matrix):
    for article_id, article_answer in enumerate(ary):
        responses.append((str(user_id), str(article_id), article_answer))
        
print(responses)
alpha = agree.AnnotationTask(responses).alpha()
# alpha = agree.AnnotationTask(responses).avg_Ao()


print(alpha)